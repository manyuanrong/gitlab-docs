---
comments: false
---

# Docs archives

CAUTION: **Warning:**
This page is in beta, many links might not work. For updates, follow
[this issue](https://gitlab.com/gitlab-com/gitlab-docs/issues/16).

Browse the archives for different GitLab versions.

## Online archives

The following archives are available online and can be browsed on
<https://docs.gitlab.com>.

### 10.6

Visit the [GitLab 10.6 docs](/10.6/) or download them and browse locally:

```sh
docker run -it --rm -p 4000:4000 registry.gitlab.com/gitlab-com/gitlab-docs:10.6
```

## Offline archives

The following archives are available offline.

### 10.5

Download them and browse locally:

```sh
docker run -it --rm -p 4000:4000 registry.gitlab.com/gitlab-com/gitlab-docs:10.5
```

### 10.4

Download them and browse locally:

```sh
docker run -it --rm -p 4000:4000 registry.gitlab.com/gitlab-com/gitlab-docs:10.4
```

### 10.3

Download them and browse locally:

```sh
docker run -it --rm -p 4000:4000 registry.gitlab.com/gitlab-com/gitlab-docs:10.3
```
